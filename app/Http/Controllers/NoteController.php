<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Note;

use App\Color;

use App\Http\Requests\NoteValidate;

class NoteController extends Controller
{
    

	/* 
	 @role: get latest notes from Database
	 
	 @comments: this is a priavte function
	 */ 
	 
	 private function getAvailableNotes() 
	 { 
	 		
	 		// get latest 20 notes 
	 		return Note::with('getColorCode')->latest()->paginate(12);

	 		

	 
	 
	 }

	 /* 
	  @role: get all the available colors from Database
	  
	  @comments: this is a priavte function
	  */ 
	  
	  private function getAvailableColors() 
	  { 
	  			

	  		// get all the available colors 
	 		return Color::all();	
	  
	  
	  }


	/* 
	 @role: display all the notes available and section to add new
	 
	 @comments: this is our landing page, here almost all the operations are done
	 */ 
	 
	 public function index() 
	 { 			
	 		// get notes	
	 		$notes = $this->getAvailableNotes();

	 		// get colors	
	 		$colors = $this->getAvailableColors();
	 		
	 		return view('notes.index')->with(compact('notes','colors'));
	 		
	 
	 }

	 /* 
	  @role: 
	  
	  @comments: 
	  */ 
	  
	  public function getEditNote($id) 
	  { 
	  		
	  		$single_note = Note::find($id);

	  		if(!$single_note)
	  		{
	  			 \Session::flash('err','Note Not Found');
	  			 return redirect()->back();
	  		}


	  		// get notes	
	 		$notes = $this->getAvailableNotes();

	 		// get colors	
	 		$colors = $this->getAvailableColors();

	 		$page_type="edit";
	 		
	 		return view('notes.index')->with(compact('notes','colors','single_note','page_type'));

	  
	  
	  }


	 /* 
	  @role: save new note if it passes the validation
	  
	  @comments: 
	  */ 
	  
	  public function save(NoteValidate $request) 
	  { 
	  		
	  		// here all the validations are passed and we can save the note

	  		$note = (new Note);

	  		$note->note_details = $request->note;
	  		$note->color_code = $request->color;

	  		$note->save();

	  		// redirect to note list page
	  		return redirect()->route('get:all:notes')->with('success','Note Created Successfully!!');
	  
	  }

	  /* 
	   @role: update existing note, as per user changes the data
	   
	   @comments: 
	   */ 
	   
	   public function update(NoteValidate $request) 
	   { 
	   
	   		// here all the validations are passed and we can save the note

	  		$note = (new Note)::find($request->id);

	  		if(!$request->id)
			$note = (new Note);		  			

	  		$note->note_details = $request->note;
	  		$note->color_code = $request->color;

	  		$note->save();

	  		// redirect to note list page
	  		return redirect()->route('get:all:notes')->with('success','Note Updated Successfully!!');
	  	
	   
	   }

	   /* 
	    @role: delete a specific note 
	    
	    @comments: 
	    */ 
	    
	    public function deleteGivenNote($id) 
	    { 
	    		
	    	$note = (new Note)::find($id);

	    	

	  		if(!$note)
			
	  		{
	  			 \Session::flash('err','OOPS !! Note Not Found');
	  			 return redirect()->back();
	  		}

	  		$note->delete();

	  		// redirect to note list page
	  		return redirect()->route('get:all:notes')->with('success','Note Deleted Successfully!!');

	    
	    
	    }


}
