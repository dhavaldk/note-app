<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoteValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'note' => 'required | min:20',
            'color'=> 'required'
        ];
    }

    /* 
     @role: to send custom validation messages to the app
     
     @comments: 
     */ 
     
     public function messages() 
     { 
        
        return [
        'note.required' => 'Note Can Not Be Empty',
        'note.min' => 'Note Needs Min 20 character',
        'color.required'=> 'Select Color Of The Note'


        ];
     
     
     }
}
