<?php


Route::get('/notes','NoteController@index')->name('get:all:notes');

Route::post('/notes','NoteController@save')->name('save:new:note');

Route::get('/notes/{id}','NoteController@getEditNote')->name('edit:added:note');

Route::post('/notes/update','NoteController@update')->name('update:added:note');

Route::get('/notes/delete/{id}','NoteController@deleteGivenNote')->name('delete:added:note');





