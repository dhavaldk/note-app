@extends('layout')


@section('main_body')

		
			
			<div class="col-lg-12 row notes">
					
				@include('session_flashes')

				@if(isset($page_type) && $page_type=='edit')
					@include('notes.edit')
				@else 
					@include('notes.create')
				@endif	

				
				<div class="col-lg-9 current_notes">
					
						<h4> Available Notes </h4>
						<hr>

						

						@if($notes)
							
							
							
						 <ul class="links">
							{{ $notes->links()}}	
						</ul>
	

						  <div class="col-lg-12 row"> 
							  @foreach($notes as $note)
							  	
								<div class="col-lg-4 note_block" style="background-color:{{$note->getColorCode->color_code}}">
										
									<div class="note_data">
										
										{{$note->note_details}}	

									</div>	
									
								
								<div class="actions">
									<a href="{{ route('edit:added:note',['id'=>$note->id])}}">
										<i class="fas fa-pencil-alt"></i> </a>
									
									<a href="{{ route('delete:added:note',['id'=>$note->id])}}" onclick="return confirm('Are You Sure You Want to Delete??')">
									<i class="fa fa-trash"></i>
									</a>	
								</div>
								</div>
								

							  @endforeach			
							
						  </div>	

						  

						@endif


				</div>


			</div>

		


@endsection


