<div class="col-lg-3 note_form">
					
					<h4> Add New Note </h4>
					<hr>
						
					@include('notes.validation_errors')

					<form action="{{ route('save:new:note')}}" method="POST">
						
						@csrf
						
						<div class="form-group">

						<textarea name="note" id=""  rows="5" class="form-control" required placeholder="Type Min 20 Characters Here">{{old('note')}}</textarea>
						</div>

						<div class="form-group">
								
							@if($colors)
								@foreach($colors as $color)

								<input type="radio" name="color" value="{{$color->id}}" required @if(old('color') && old('color')==$color->id) checked @endif> <div class="round_color" style="background-color:{{$color->color_code}}"></div>{{$color->color_name}} <br>

								@endforeach	


							@endif	

						</div>

						<div class="form-group">
								
							<button class="btn btn-primary save" type="submit">Save</button>	
							<button class="btn btn-danger clear">Clear Selection</button>	


						</div>	
					</form>


				</div>	