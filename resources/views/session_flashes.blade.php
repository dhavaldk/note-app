<div class="col-lg-12">

<ul>
	@if(\Session::get('err'))
	<li class="alert alert-danger">{{ \Session::get('err')}}</li>
	@endif

	@if(\Session::get('success'))
	<li class="alert alert-success">{{ \Session::get('success')}}</li>
	@endif
	

</ul>

</div>

<div class="clear"></div>