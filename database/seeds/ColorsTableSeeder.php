<?php

use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	DB::table('colors')->truncate();

    	$colors = [

    	[
    		'color_name' => 'Black',
    		'color_code' => '#000'

    	],
    	[
    		'color_name' => 'Brown',
    		'color_code' => '#c4a556'

    	],
    	[
    		'color_name' => 'Orange',
    		'color_code' => '#ffb900'

    	],
    	[
    		'color_name' => 'Red',
    		'color_code' => '#cc3908'

    	],
    	[
    		'color_name' => 'Blue',
    		'color_code' => '#006cc4'

    	]

    ];

    DB::table('colors')->insert($colors);		



    }
}
